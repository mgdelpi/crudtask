package com.cl.crudtask;

import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.cl.crudtask.pojo.request.TaskDTO;

@SpringBootTest
public class CrudTaskApplicationTest {

	@Test
	public void insertTask() throws SQLException {
		TaskDTO task = new TaskDTO();
		task.setDescription("TAREA PRUEBA");

		Assert.assertEquals("TAREA PRUEBSA", task.getDescription());
	}

	@Test
	public void helloWorld() {
		String value = "TAREA PRUEBA";

		Assert.assertEquals("TAREA PRUEBA", value);
	}
}
