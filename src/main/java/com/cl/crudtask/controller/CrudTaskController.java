package com.cl.crudtask.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cl.crudtask.pojo.ResponseMicroService;
import com.cl.crudtask.pojo.request.TaskDTO;
import com.cl.crudtask.pojo.request.TaskUpdateDTO;
import com.cl.crudtask.service.CrudTaskService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class CrudTaskController {

	@Autowired
	private CrudTaskService crudTaskService;

	private Logger logger = LoggerFactory.getLogger(CrudTaskController.class);

	@ControllerAdvice
	public class ErrorHandler {
		@ExceptionHandler(MethodArgumentNotValidException.class)
		public ResponseEntity<Object> methodCatchResponseException(MethodArgumentNotValidException ex) {

			Map<String, String> errors = new HashMap<>();

			for (FieldError error : ex.getBindingResult().getFieldErrors()) {
				errors.put(error.getField(), error.getDefaultMessage());
			}
			return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
		}

	}

	/**
	 * @author mdelpiar
	 * @date 14-03-2021
	 * @param
	 * @return taskTO
	 */
	@GetMapping(value = "/findTask", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Servicio para listar tareas")
	public ResponseEntity<Object> findTask() {

		ResponseMicroService<Object> response = new ResponseMicroService<Object>();

		try {
			response = crudTaskService.findTask();
		} catch (Exception e) {
			logger.error("findTask", e);
			return new ResponseEntity<>("Service Error", HttpStatus.SERVICE_UNAVAILABLE);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	/**
	 * @author mdelpiar
	 * @date 14-03-2021
	 * @param taskDTO
	 * @return
	 */
	@PostMapping(value="/insert", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Servicio para agregar una tarea")
	public ResponseEntity<Object> insertTask(@Valid @RequestBody TaskDTO task) {

		ResponseMicroService<Object> response = new ResponseMicroService<Object>();

		try {
			response = crudTaskService.insertTask(task);
		} catch (Exception e) {
			logger.error("insertTask", e);
			return new ResponseEntity<>("Service Error", HttpStatus.SERVICE_UNAVAILABLE);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	/**
	 * @author mdelpiar
	 * @date 14-03-2021
	 * @param taskUpdateTO
	 * @return 
	 */
	@DeleteMapping(value = "/delete/{taskId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Servicio para remover una tarea")
	public ResponseEntity<Object> deleteTaskById(
			@Valid @Min(message = "TaskId debe ser numerico", value = 1) @PathVariable int taskId) {

		ResponseMicroService<Object> response = new ResponseMicroService<Object>();

		try {
			response = crudTaskService.deleteTaskById(taskId);
		} catch (SQLException e) {
			logger.error("deleteTaskById", e);
			return new ResponseEntity<>("Service Error", HttpStatus.SERVICE_UNAVAILABLE);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * @author mdelpiar
	 * @date 14-03-2021
	 * @param
	 * @return ProjectTO
	 */
	@PostMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Servicio para editar una tarea")
	public ResponseEntity<Object> updateTaskById(@Valid @RequestBody TaskUpdateDTO task) {

		ResponseMicroService<Object> response = new ResponseMicroService<Object>();

		try {
			response = crudTaskService.updateTaskById(task);
		} catch (SQLException e) {
			logger.error("updateTaskById", e);
			return new ResponseEntity<>("Service Error", HttpStatus.SERVICE_UNAVAILABLE);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
