package com.cl.crudtask.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.cl.crudtask.config.DataSource;
import com.cl.crudtask.pojo.request.TaskDTO;
import com.cl.crudtask.pojo.request.TaskUpdateDTO;
import com.cl.crudtask.pojo.response.TaskTO;

public class CrudTaskRepository {

	private final static boolean VALID = true;

	public static List<TaskTO> findTask() throws SQLException {
		List<TaskTO> listTask = new ArrayList<TaskTO>();

		try (Connection con = DataSource.getConnection();
				PreparedStatement pst = con.prepareStatement("select * from find_task()");) {
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					TaskTO item = new TaskTO();
					item.setTaskId(rs.getInt("task_id"));
					item.setDescription(rs.getString("description"));
					item.setCreationDate(rs.getTimestamp("creation_date"));
					item.setValid(rs.getBoolean("valid"));
					listTask.add(item);
				}
			}
		}
		return listTask;

	}

	public static int insertTask(TaskDTO task) throws SQLException {
		int id = 0;

		try (Connection con = DataSource.getConnection();
				PreparedStatement pst = con.prepareStatement("select * from insert_task(?,?)");) {
			pst.setString(1, task.getDescription());
			pst.setBoolean(2, VALID);
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					id = rs.getInt(1);
				}
			}
		}
		return id;
	}

	public static int findTaskById(int taskId) throws SQLException {
		int id = 0;

		try (Connection con = DataSource.getConnection();
				PreparedStatement pst = con.prepareStatement("select * from find_task_by_id(?)");) {
			pst.setInt(1, taskId);
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					id = rs.getInt(1);
				}
			}
		}
		return id;
	}

	public static int updateTaskById(TaskUpdateDTO task) throws SQLException {
		int id = 0;

		try (Connection con = DataSource.getConnection();
				PreparedStatement pst = con.prepareStatement("select * from update_task_by_id(?,?,?)");) {
			pst.setInt(1, task.getTaskId());
			pst.setString(2, task.getDescription());
			pst.setBoolean(3, task.isValid());
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					id = rs.getInt(1);
				}
			}
		}
		return id;
	}
	
	public static int deleteTaskById(int idTask) throws SQLException {
		int id = 0;

		try (Connection con = DataSource.getConnection();
				PreparedStatement pst = con.prepareStatement("select * from delete_task_by_id(?)");) {
			pst.setInt(1, idTask);
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					id = rs.getInt(1);
				}
			}
		}
		return id;
	}

}
