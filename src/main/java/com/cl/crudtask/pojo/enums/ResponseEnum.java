package com.cl.crudtask.pojo.enums;

public enum ResponseEnum {

	STATUS_SUCCESS(200, "Request ok"), NOT_FOUND(404, "Not Found"), ERROR_SERVICE(503, "Service Error"),
	ERROR_CANT_NOT_INSERT(101, "Error al insertar"), NOT_MOVEMENTS(109, "No tiene movimientos"),
	ERROR_DELETE(114, "Error al borrar"), ERROR_UPDATE(115, "Error al actualizar"),
	NO_CONTENT(204, "La tarea no existe");

	ResponseEnum(int code, String descripcion) {
		this.code = code;
		this.description = descripcion;
	}

	private int code;
	private String description;

	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static ResponseEnum getMessageByCode(int cod) {
		for (ResponseEnum item : ResponseEnum.values()) {
			if (item.code == cod)
				return item;
		}
		return null;

	}
}
