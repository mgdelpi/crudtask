package com.cl.crudtask.pojo.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class TaskDTO {

	@NotEmpty(message = "El campo description no puede estar vacio")
	@NotNull(message = "El campo description no puede ser nulo")
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
