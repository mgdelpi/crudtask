package com.cl.crudtask.pojo.request;

import javax.validation.constraints.Min;

public class TaskUpdateDTO {

	@Min(value = 0, message = "El campo taskId deber ser informado y numerico")
	private int taskId;

	private String description;
	private boolean valid;

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

}
