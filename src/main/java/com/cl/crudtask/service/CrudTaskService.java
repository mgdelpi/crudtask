package com.cl.crudtask.service;

import java.sql.SQLException;

import com.cl.crudtask.pojo.ResponseMicroService;
import com.cl.crudtask.pojo.request.TaskDTO;
import com.cl.crudtask.pojo.request.TaskUpdateDTO;

public interface CrudTaskService {

	public ResponseMicroService<Object> findTask() throws SQLException;

	public ResponseMicroService<Object> insertTask(TaskDTO task) throws SQLException;

	public ResponseMicroService<Object> deleteTaskById(int taskId) throws SQLException;

	public ResponseMicroService<Object> updateTaskById(TaskUpdateDTO task) throws SQLException;

}
