package com.cl.crudtask.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cl.crudtask.pojo.ResponseMicroService;
import com.cl.crudtask.pojo.enums.ResponseEnum;
import com.cl.crudtask.pojo.request.TaskDTO;
import com.cl.crudtask.pojo.request.TaskUpdateDTO;
import com.cl.crudtask.pojo.response.TaskTO;
import com.cl.crudtask.repository.CrudTaskRepository;
import com.cl.crudtask.service.CrudTaskService;

@Service
public class CrudTaskServiceImpl implements CrudTaskService {

	@Override
	public ResponseMicroService<Object> findTask() throws SQLException {
		int code = 200;

		ResponseMicroService<Object> response = new ResponseMicroService<Object>();

		List<TaskTO> listTask = CrudTaskRepository.findTask();
		if (listTask.isEmpty()) {
			code = 109;
		} else {
			response.setResponse(listTask);
		}

		response.setCode(code);
		response.setMessage(ResponseEnum.getMessageByCode(code).getDescription());
		return response;
	}

	@Override
	public ResponseMicroService<Object> insertTask(TaskDTO task) throws SQLException {
		int code = 200;

		ResponseMicroService<Object> response = new ResponseMicroService<Object>();

		int idTask = CrudTaskRepository.insertTask(task);
		if (idTask == 0) {
			code = 101;
		}

		response.setCode(code);
		response.setMessage(ResponseEnum.getMessageByCode(code).getDescription());
		return response;
	}

	@Override
	public ResponseMicroService<Object> deleteTaskById(int taskId) throws SQLException {
		int code = 200;

		ResponseMicroService<Object> response = new ResponseMicroService<Object>();

		int id = CrudTaskRepository.findTaskById(taskId);
		if (id == 0) {
			code = 204;
		} else {
			int deleteTask = CrudTaskRepository.deleteTaskById(taskId);
			if (deleteTask == 0) {
				code = 114;
			}
		}

		response.setCode(code);
		response.setMessage(ResponseEnum.getMessageByCode(code).getDescription());
		return response;
	}

	@Override
	public ResponseMicroService<Object> updateTaskById(TaskUpdateDTO task) throws SQLException {
		int code = 200;

		ResponseMicroService<Object> response = new ResponseMicroService<Object>();

		int id = CrudTaskRepository.findTaskById(task.getTaskId());
		if (id == 0) {
			code = 204;
		} else {
			int updateTask = CrudTaskRepository.updateTaskById(task);
			if (updateTask == 0) {
				code = 115;
			}
		}

		response.setCode(code);
		response.setMessage(ResponseEnum.getMessageByCode(code).getDescription());
		return response;

	}

}
