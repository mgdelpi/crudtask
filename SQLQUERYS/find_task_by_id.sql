CREATE OR REPLACE FUNCTION public.find_task_by_id(integer)
 RETURNS SETOF integer
 LANGUAGE plpgsql
AS $function$
BEGIN
	return query select t.task_id from task t where t.task_id = $1;
END
$function$
;
