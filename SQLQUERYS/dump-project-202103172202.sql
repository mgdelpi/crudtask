PGDMP                         y           project    12.3    12.3     	           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            
           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    131497    project    DATABASE     �   CREATE DATABASE project WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Chile.1252' LC_CTYPE = 'Spanish_Chile.1252';
    DROP DATABASE project;
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3            �            1255    131507    delete_task_by_id(integer)    FUNCTION     �   CREATE FUNCTION public.delete_task_by_id(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
DECLARE 
	id int;
BEGIN
	id=1;
	DELETE FROM task WHERE task_id =$1;
	return id;
END;
$_$;
 1   DROP FUNCTION public.delete_task_by_id(integer);
       public          postgres    false    3            �            1255    131513    find_task()    FUNCTION     "  CREATE FUNCTION public.find_task() RETURNS TABLE(task_id integer, description character varying, creation_date timestamp with time zone, valid boolean)
    LANGUAGE plpgsql
    AS $$
BEGIN
  	return query select t.task_id, t.description, t.creation_date, t."valid" from task t;
END
$$;
 "   DROP FUNCTION public.find_task();
       public          postgres    false    3            �            1255    131511    find_task_by_id(integer)    FUNCTION     �   CREATE FUNCTION public.find_task_by_id(integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $_$
BEGIN
	return query select t.task_id from task t where t.task_id = $1;
END
$_$;
 /   DROP FUNCTION public.find_task_by_id(integer);
       public          postgres    false    3            �            1255    131509 '   insert_task(character varying, boolean)    FUNCTION     &  CREATE FUNCTION public.insert_task(character varying, boolean) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
declare 
	id int;
BEGIN
	id=0;
	INSERT INTO task 
			(description,"valid",creation_date)
		VALUES ($1,$2,current_timestamp) RETURNING task_id INTO id;
	return id;
END;
$_$;
 >   DROP FUNCTION public.insert_task(character varying, boolean);
       public          postgres    false    3            �            1255    131515 6   update_task_by_id(integer, character varying, boolean)    FUNCTION     ;  CREATE FUNCTION public.update_task_by_id(integer, character varying, boolean) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
DECLARE 
id int;
BEGIN
	id=0;
	UPDATE public.task 
	set description=$2, "valid"=$3, last_update=current_timestamp WHERE task_id= $1 returning task_id into id;
	return id;
END
$_$;
 M   DROP FUNCTION public.update_task_by_id(integer, character varying, boolean);
       public          postgres    false    3            �            1259    131500    task    TABLE     �   CREATE TABLE public.task (
    task_id integer NOT NULL,
    description character varying(250) NOT NULL,
    creation_date timestamp with time zone NOT NULL,
    valid boolean NOT NULL,
    last_update timestamp without time zone
);
    DROP TABLE public.task;
       public         heap    postgres    false    3            �            1259    131498    task_task_id_seq    SEQUENCE     �   CREATE SEQUENCE public.task_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.task_task_id_seq;
       public          postgres    false    203    3                       0    0    task_task_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.task_task_id_seq OWNED BY public.task.task_id;
          public          postgres    false    202            �
           2604    131503    task task_id    DEFAULT     l   ALTER TABLE ONLY public.task ALTER COLUMN task_id SET DEFAULT nextval('public.task_task_id_seq'::regclass);
 ;   ALTER TABLE public.task ALTER COLUMN task_id DROP DEFAULT;
       public          postgres    false    202    203    203                      0    131500    task 
   TABLE DATA                 public          postgres    false    203                       0    0    task_task_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.task_task_id_seq', 4, true);
          public          postgres    false    202            �
           2606    131505    task task_pk 
   CONSTRAINT     O   ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pk PRIMARY KEY (task_id);
 6   ALTER TABLE ONLY public.task DROP CONSTRAINT task_pk;
       public            postgres    false    203               �   x���v
Q���W((M��L�+I,�Vs�	uV�0�QPqruT0T2��u�uM���ʔ���(����hZsy0�n�^C�s�S�+P'�c�5��s�	�P��|����L���M�L���Mqx��� K=#C#KS�o�� eS_0          	           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            
           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    131497    project    DATABASE     �   CREATE DATABASE project WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Chile.1252' LC_CTYPE = 'Spanish_Chile.1252';
    DROP DATABASE project;
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3            �            1255    131507    delete_task_by_id(integer)    FUNCTION     �   CREATE FUNCTION public.delete_task_by_id(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
DECLARE 
	id int;
BEGIN
	id=1;
	DELETE FROM task WHERE task_id =$1;
	return id;
END;
$_$;
 1   DROP FUNCTION public.delete_task_by_id(integer);
       public          postgres    false    3            �            1255    131513    find_task()    FUNCTION     "  CREATE FUNCTION public.find_task() RETURNS TABLE(task_id integer, description character varying, creation_date timestamp with time zone, valid boolean)
    LANGUAGE plpgsql
    AS $$
BEGIN
  	return query select t.task_id, t.description, t.creation_date, t."valid" from task t;
END
$$;
 "   DROP FUNCTION public.find_task();
       public          postgres    false    3            �            1255    131511    find_task_by_id(integer)    FUNCTION     �   CREATE FUNCTION public.find_task_by_id(integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $_$
BEGIN
	return query select t.task_id from task t where t.task_id = $1;
END
$_$;
 /   DROP FUNCTION public.find_task_by_id(integer);
       public          postgres    false    3            �            1255    131509 '   insert_task(character varying, boolean)    FUNCTION     &  CREATE FUNCTION public.insert_task(character varying, boolean) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
declare 
	id int;
BEGIN
	id=0;
	INSERT INTO task 
			(description,"valid",creation_date)
		VALUES ($1,$2,current_timestamp) RETURNING task_id INTO id;
	return id;
END;
$_$;
 >   DROP FUNCTION public.insert_task(character varying, boolean);
       public          postgres    false    3            �            1255    131515 6   update_task_by_id(integer, character varying, boolean)    FUNCTION     ;  CREATE FUNCTION public.update_task_by_id(integer, character varying, boolean) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
DECLARE 
id int;
BEGIN
	id=0;
	UPDATE public.task 
	set description=$2, "valid"=$3, last_update=current_timestamp WHERE task_id= $1 returning task_id into id;
	return id;
END
$_$;
 M   DROP FUNCTION public.update_task_by_id(integer, character varying, boolean);
       public          postgres    false    3            �            1259    131500    task    TABLE     �   CREATE TABLE public.task (
    task_id integer NOT NULL,
    description character varying(250) NOT NULL,
    creation_date timestamp with time zone NOT NULL,
    valid boolean NOT NULL,
    last_update timestamp without time zone
);
    DROP TABLE public.task;
       public         heap    postgres    false    3            �            1259    131498    task_task_id_seq    SEQUENCE     �   CREATE SEQUENCE public.task_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.task_task_id_seq;
       public          postgres    false    203    3                       0    0    task_task_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.task_task_id_seq OWNED BY public.task.task_id;
          public          postgres    false    202            �
           2604    131503    task task_id    DEFAULT     l   ALTER TABLE ONLY public.task ALTER COLUMN task_id SET DEFAULT nextval('public.task_task_id_seq'::regclass);
 ;   ALTER TABLE public.task ALTER COLUMN task_id DROP DEFAULT;
       public          postgres    false    202    203    203                      0    131500    task 
   TABLE DATA                 public          postgres    false    203                       0    0    task_task_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.task_task_id_seq', 4, true);
          public          postgres    false    202            �
           2606    131505    task task_pk 
   CONSTRAINT     O   ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pk PRIMARY KEY (task_id);
 6   ALTER TABLE ONLY public.task DROP CONSTRAINT task_pk;
       public            postgres    false    203           