CREATE OR REPLACE FUNCTION public.delete_task_by_id(integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
DECLARE 
	id int;
BEGIN
	id=1;
	DELETE FROM task WHERE task_id =$1;
	return id;
END;
$function$
;
