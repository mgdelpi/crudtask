CREATE OR REPLACE FUNCTION public.update_task_by_id(integer, character varying, boolean)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
DECLARE 
id int;
BEGIN
	id=0;
	UPDATE public.task 
	set description=$2, "valid"=$3, last_update=current_timestamp WHERE task_id= $1 returning task_id into id;
	return id;
END
$function$
;
