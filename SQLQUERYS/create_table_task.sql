-- public.task definition

-- Drop table

-- DROP TABLE public.task;

CREATE TABLE public.task (
	task_id serial NOT NULL,
	description varchar(250) NOT NULL,
	creation_date timestamptz NOT NULL,
	"valid" bool NOT NULL,
	last_update timestamp NULL,
	CONSTRAINT task_pk PRIMARY KEY (task_id)
);