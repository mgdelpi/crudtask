CREATE OR REPLACE FUNCTION public.find_task()
 RETURNS TABLE(task_id integer, description character varying, creation_date timestamp with time zone, valid boolean)
 LANGUAGE plpgsql
AS $function$
BEGIN
  	return query select t.task_id, t.description, t.creation_date, t."valid" from task t;
END
$function$
;
