CREATE OR REPLACE FUNCTION public.insert_task(character varying, boolean)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
declare 
	id int;
BEGIN
	id=0;
	INSERT INTO task 
			(description,"valid",creation_date)
		VALUES ($1,$2,current_timestamp) RETURNING task_id INTO id;
	return id;
END;
$function$
;
